# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install lfric-meta
#
# You can edit this file again by typing:
#
#     spack edit lfric-meta
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack.package import *


class LfricXiosMeta(BundlePackage):
    """Dependencies of LFRic."""

    # FIXME: Add a proper url for your package's homepage here.
    # homepage = "https://www.example.com"
    # There is no URL since there is no code to download.

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers("github_user1", "github_user2")

    # FIXME: Add the SPDX identifier of the project's license below.
    # See https://spdx.org/licenses/ for a list. Upon manually verifying
    # the license, set checked_by to your Github username.
    # license("UNKNOWN", checked_by="github_user1")

    version("1.0")

    # Dependencies
    depends_on("mpi")
    depends_on("hdf5+mpi")
    depends_on("netcdf-c+mpi")
    depends_on("netcdf-fortran ^netcdf-c+mpi")

    depends_on("yaxt")
    # depends_on("xios@2.5")
    depends_on("py-jinja2")
    depends_on("py-psyclone@2.3.1")
    depends_on("rose-picker")


    # Set up environment paths
    def setup_run_environment(self, run_env):
        spec = self.spec

        run_env.unset("FFLAGS")
        run_env.unset("LDFLAGS")

        # Compiler agnostic env vars
        run_env.set("FC", "ftn")
        run_env.set("LDMPI", "ftn")
        run_env.set("FPP", "cpp -traditional-cpp")
        run_env.set("LFRIC_TARGET_PLATFORM", "meto-xc40")

        # NetCDF
        dir = spec["netcdf-c"].prefix.include
        run_env.prepend_path("FFLAGS", f"-I{dir}")
        dir = spec["netcdf-c"].prefix.lib
        run_env.prepend_path("LDFLAGS", f"-L{dir} ", " ")

        # HDF5
        dir = spec["hdf5"].prefix.include
        run_env.prepend_path("FFLAGS", f"-I{dir} ", " ")
        dir = spec["hdf5"].prefix.lib
        run_env.prepend_path("LDFLAGS", f"-L{dir} ", " ")

        # XIOS
        # dir = spec["xios"].prefix.include
        # run_env.prepend_path("FFLAGS", f"-I{dir} ", " ")
        # dir = spec["xios"].prefix.lib
        # run_env.prepend_path("LDFLAGS", f"-L{dir} ", " ")
        # run_env.prepend_path("LD_LIBRARY_PATH", f"{dir} ")

        # YAXT
        dir = spec["yaxt"].prefix.include
        run_env.prepend_path("FFLAGS", f"-I{dir} ", " ")
        dir = spec["yaxt"].prefix.lib
        run_env.prepend_path("LDFLAGS", f"-L{dir} ", " ")
        run_env.prepend_path("LD_LIBRARY_PATH", f"{dir} ")
